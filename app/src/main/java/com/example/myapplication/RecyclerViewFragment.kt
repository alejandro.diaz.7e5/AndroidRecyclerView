package com.example.myapplication

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.databinding.FragmentRecyclerViewBinding

class RecyclerViewFragment :Fragment(),  OnClickListener {
    private lateinit var userAdapter: UserAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentRecyclerViewBinding

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentRecyclerViewBinding.inflate(inflater, container, false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userAdapter = UserAdapter(getUsers(), this)
        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = userAdapter
        }
    }

    private fun getUsers(): MutableList<User>{
        val users = mutableListOf<User>()
        users.add(User(1, "Potatron", "https://global.discourse-cdn.com/eveonline/original/3X/6/3/63db795497646f2b913665554a383ae50621fed7.jpeg"))
        return users
    }

    override fun onClick(user: User) {
        parentFragmentManager.setFragmentResult(
            "User", bundleOf("User" to user)
        )
        /*parentFragmentManager.beginTransaction().apply {
            replace(R.id.nav_host_fragment,DetailFragment())
            setReorderingAllowed(true)
            addToBackStack(null)
            commit()
        }*/

        val action = RecyclerViewFragmentDirections.actionRecyclerViewFragmentToDetailFragment(user.id.toString())
        findNavController().navigate(action)



    }

}