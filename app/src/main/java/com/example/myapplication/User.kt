package com.example.myapplication

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
    data class User(val id: Long, var name: String, var url: String): Parcelable

