package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.myapplication.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        /*supportFragmentManager.beginTransaction().apply {
            replace(R.id.nav_host_fragment,RecyclerViewFragment())
            setReorderingAllowed(true)
            addToBackStack("name") // name can be null
            commit()
        }*/

    }
}
