package com.example.myapplication

interface OnClickListener {
    fun onClick(user: User)
}